﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МКР_2
{
    public partial class Form1 : Form
    {
        VSTUP_User[] users_array = new VSTUP_User[5];
        public Form1()
        {
            InitializeComponent();
            users_array[0] = new VSTUP_User("Галь", "Чоловіча", 2022, 180);
            users_array[1] = new VSTUP_User("Вогар", "Чоловіча", 2022, 182);
            users_array[2] = new VSTUP_User("Глуханич", "Чоловіча", 2022, 164);
            users_array[3] = new VSTUP_User("Балко", "Чоловіча", 2022, 185);
            users_array[4] = new VSTUP_User("Півкач", "Чоловіча", 2022, 175);
            dataGridView1.RowCount = users_array.Length;
            dataGridView1.ColumnCount = 4;
            RenderData();
        }
        public void RenderData()
        {
            for(int i = 0; i<users_array.Length; i++)
            {
                dataGridView1.Rows[i].Cells[0].Value = users_array[i].surname;
                dataGridView1.Rows[i].Cells[1].Value = users_array[i].sex;
                dataGridView1.Rows[i].Cells[2].Value = users_array[i].school_year;
                dataGridView1.Rows[i].Cells[3].Value = users_array[i].average_score;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < users_array.Length; i++)
            {
                if (users_array[i].surname == textBox1.Text)
                {
                    label2.Text = $"Результат: Прізвище: {users_array[i].surname} Стать: {users_array[i].sex} Рік закінчення школи {users_array[i].school_year} Середній бал {users_array[i].school_year}";
                    break;
                }
                if (users_array[i].surname != textBox1.Text && i+1 == users_array.Length)
                {
                    label2.Text = "Результат: Користувача не знайдено";
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int found_users = 0;
            int school = (int)year.Value;
            decimal points = score.Value;
            for(int i = 0;i<users_array.Length;i++)
            {
                if (users_array[i].school_year == school && users_array[i].average_score > points)
                {
                    found_users++;
                }
            }
            label5.Text = $"Кількість абітурієнтів:{found_users}";
        }
    }
}
