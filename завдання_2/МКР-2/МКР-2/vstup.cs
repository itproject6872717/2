﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace МКР_2
{
    public class VSTUP_User
    {
        public string surname { get; set; }
        public string sex { get; set; }
        public int school_year { get; set; }
        public decimal average_score { get;set; }

        public VSTUP_User(string surname, string sex, int school_year, decimal score) { 
            this.surname = surname;
            this.sex = sex;
            this.school_year = school_year;
            this.average_score = score;
        }
    }
}
