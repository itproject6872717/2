﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Завдання_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.RowCount = 1;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = (int)numericUpDown1.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int nums = 0;
            for(int i = 0;i<dataGridView1.Columns.Count;i++)
            {
                if (i == 0)
                {
                    if(Convert.ToDouble(dataGridView1.Rows[0].Cells[1].Value) < Convert.ToDouble(dataGridView1.Rows[0].Cells[0].Value)){
                        nums++;
                    }
                }
                else {
                    if (i + 1 == dataGridView1.Columns.Count)
                    {
                        if(Convert.ToDouble(dataGridView1.Rows[0].Cells[i].Value) > Convert.ToDouble(dataGridView1.Rows[0].Cells[i - 1].Value))
                        {
                            nums++;
                        }
                        break;
                    }
                    double val_1 = Convert.ToDouble(dataGridView1.Rows[0].Cells[i].Value);
                    double val_2 = Convert.ToDouble(dataGridView1.Rows[0].Cells[i - 1].Value);
                    double val_3 = Convert.ToDouble(dataGridView1.Rows[0].Cells[i + 1].Value);
                    if (val_1 > val_2 && val_1 > val_3)
                    {
                        nums++;
                    }
                }
            }
            label2.Text = $"Результат: {nums}";
        }
    }
}
